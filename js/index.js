$(function(){
    $("[data-toggle='tooltip']").tooltip();
    $('.carousel').carousel({
        interval:2000
    });
});

$(function () {
      $('[data-toggle="popover"]').popover()
})

$('#contacto').on('show.bs.modal', function(e){
    console.log('el modal se esta mostrando');

    $('#contactoBtn').removeClass('btn-outlone-success');
    $('#contactoBtn').addClass('btn-primary');
    $('#contactoBtn').prop('disabled', true);
});


$('#contacto').on('shown.bs.modal', function(e){
    console.log('el modal se mostro');
});

$('#contacto').on('hide.bs.modal', function(e){
    console.log('el modal se oculta');
});

$('#contacto').on('hidden.bs.modal', function(e){
    console.log('el modal se oculto');
    $('#contactoBtn').prop('disabled', false);
    $('#contactoBtn').addClass('btn-outlone-success');
    $('#contactoBtn').removeClass('btn-primary');
});

